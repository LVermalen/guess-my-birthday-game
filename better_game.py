from random import randint

name = input("Hi! What is your name? ")


for guess_num in range(1, 6):
    month = (randint(1, 12))
    year = (randint(1924, 2004))
    print("Guess", guess_num, name, " Were you born in ", 
    month, "/", year, " (Yes or No?)")
    answer = input("Your answer: ")
    if answer == "yes":
        print("I knew it!")
        exit()

    elif answer == "no":
        print("Drat! Let me try again.")
        if guess_num == 5:
            print("I have better things to do, bye!")
            exit()


